package com.example.busoffloader.data

import com.example.busoffloader.data.model.BankingDeposit
import com.example.busoffloader.data.model.Payment
import com.example.busoffloader.data.model.PaymentDescription
import com.example.busoffloader.data.model.Vehicle
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import retrofit2.http.Field
import retrofit2.http.GET
import retrofit2.http.Query
import java.math.BigDecimal

interface OffloadDataSource {
    fun newPaymentTransaction(transactionType: String, paymentDescriptionId: Long,
                              vehicleId: Long?, amount: BigDecimal): Single<Payment>


    fun paymentTransactions(createdById: Long?,
                            vehicleId: Long?): Single<List<Payment>>

    fun paymentsDescriptions(): Single<List<PaymentDescription>>
}

interface BankingDepositDataSource {
    fun newBankingDeposit(description: String, amount: BigDecimal): Single<BankingDeposit>

    fun bankingDeposits(): Single<List<BankingDeposit>>
}

interface VehicleDataSource {
    fun vehicles(): Single<List<Vehicle>>
}
