package com.example.busoffloader.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
data class PaymentDescription(val id: Long,
                              val name: String,
                              val description: String?,
                              val status: String,
                              @Json(name = "date_created") val dateCreated: String,
                              @Json(name = "date_modified") val dateModified: String,
                              @Json(name = "created_by") val createdBY: Long?,
                              @Json(name = "modified_by") val modifiedBy: Long?){
    override fun toString(): String = name.capitalize(Locale.getDefault())
}