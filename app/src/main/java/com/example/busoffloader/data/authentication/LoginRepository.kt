package com.example.busoffloader.data.authentication

import com.example.busoffloader.data.model.AuthJWTDetails
import com.example.busoffloader.data.model.User
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Single

/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */

class LoginRepository(private val dataSource: AuthDataSource,
                      private val localAuthStore: AuthJWTKeysStore,
                      private val localDataStore: LocalDataSource) {

    fun login(username: String, password: String): Single<AuthJWTDetails> = dataSource
        .login(username = username,
               password = password)
        .doOnSuccess {
            localAuthStore.putNewAuthKeyDetails(keys = it)
        }

    fun getUser(): Flowable<User> =
        Flowable
            .concat(Maybe.fromCallable { localDataStore.getUserDetails() }
                        .toFlowable(),
                    dataSource.user()
                        .doOnSuccess {
                            localDataStore.storeUserDetails(it)
                        }
                        .toFlowable())

    fun isLoggedIn(): Boolean {
        return localAuthStore.doAuthKeyDetailsExist()
    }

    fun logout() {
        localAuthStore.invalidateAuthDetails()
        localDataStore.clearUserDetails()
    }
}