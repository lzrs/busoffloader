package com.example.busoffloader.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AuthJWTDetails(@Json(name = "refresh") val refreshKey: String = "", // default blank
                          @Json(name = "access") val accessKey: String = "")  // default blank