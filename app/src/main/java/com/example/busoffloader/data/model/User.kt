package com.example.busoffloader.data.model

import android.os.Parcel
import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class User(val id: Long,
                val username: String,
                val email: String,
                @Json(name = "first_name") val firstName: String,
                @Json(name = "last_name") val lastName: String,
                @Json(name = "last_login") val lastLogin: String,
                @Json(name = "is_staff") val isStaff: Boolean,
                @Json(name = "is_active") val isActive: Boolean,
                @Json(name = "date_joined") val dateJoined: String): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readString()!!) {
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(username)
        parcel.writeString(email)
        parcel.writeString(firstName)
        parcel.writeString(lastName)
        parcel.writeString(lastLogin)
        parcel.writeByte(if (isStaff) 1 else 0)
        parcel.writeByte(if (isActive) 1 else 0)
        parcel.writeString(dateJoined)
    }

    override fun describeContents(): Int {
        return 0
    }
}