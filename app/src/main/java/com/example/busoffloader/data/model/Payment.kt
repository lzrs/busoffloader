package com.example.busoffloader.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.math.BigDecimal

@JsonClass(generateAdapter = true)
data class Payment(val id: Long,
                   @Json(name = "transaction_type") val transactionType: String,
                   val amount: BigDecimal,
                   val notes: String?,
                   val status: String,
                   @Json(name = "date_created") val dateCreated: String,
                   @Json(name = "date_modified") val dateModified: String,
                   @Json(name = "payment_description") val paymentDescriptionId: Long,
                   @Json(name = "vehicle") val vehicleId: Long,
                   @Json(name = "created_by") val createdBy: Long?,
                   @Json(name = "modified_by") val modifiedBy: Long?) {
    var vehicleRep: Vehicle? = null
    var paymentDescription: PaymentDescription? = null
}