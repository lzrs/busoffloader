package com.example.busoffloader.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Vehicle(val id: Long,
                   @Json(name = "fleet_number") val fleetNumber: String,
                   val registration: String,
                   val capacity: Int,
                   val status: String,
                   @Json(name = "date_created") val dateCreated: String,
                   @Json(name = "date_modified") val dateModified: String,
                   val owner: Long,
                   @Json(name = "created_by") val createdBy: Long?,
                   @Json(name = "modified_by") val modifiedBy: Long?){
    override fun toString(): String = "$registration - $fleetNumber"
}