package com.example.busoffloader.data.authentication

import com.example.busoffloader.data.model.AuthJWTDetails
import com.example.busoffloader.data.model.User
import com.example.busoffloader.data.remote.HttpClientAuthProvider
import io.reactivex.rxjava3.core.Single

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */

interface AuthDataSource {
    fun login(username: String, password: String): Single<AuthJWTDetails>

    fun user(): Single<User>

//   fun logout()
}

interface LocalDataSource {
    fun storeUserDetails(user: User)

    fun getUserDetails(): User?

    fun clearUserDetails()
}

interface AuthJWTKeysStore: HttpClientAuthProvider {
    fun putNewAuthKeyDetails(keys: AuthJWTDetails)

    fun invalidateAuthDetails(): Unit

    fun doAuthKeyDetailsExist(): Boolean
}
