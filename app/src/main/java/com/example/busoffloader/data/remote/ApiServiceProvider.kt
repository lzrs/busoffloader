package com.example.busoffloader.data.remote

import com.example.busoffloader.BuildConfig
import com.example.busoffloader.data.BigDecimalAdapter
import com.example.busoffloader.data.model.AuthJWTDetails
import com.squareup.moshi.Moshi
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.create

interface HttpClientAuthProvider {
    fun getAuthKeyDetails(): AuthJWTDetails
}


class ApiServiceProvider(private val jwtAuthLink: HttpClientAuthProvider,
                         private val onAuthExpired: () -> Unit) {

    private val baseURL = if (BuildConfig.DEBUG) "http://68.183.21.153:9500/" else "http://68.183.21.153:9500/"

    private val _moshi = Moshi.Builder().add(BigDecimalAdapter).build()

    private fun newRetrofitInstance(): Retrofit = Retrofit.Builder()
        .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
        .addConverterFactory(MoshiConverterFactory.create(_moshi))
        .baseUrl(baseURL)
        .client(newOkHttpClient())
        .validateEagerly(true)
        .build()

    private fun newOkHttpClient(): OkHttpClient = OkHttpClient.Builder()
        .authenticator { _, response ->
            when {
                response.request.header("Authorization") != null -> {
                    // TODO: refresh auth token
                    onAuthExpired.invoke()
                    null
                }
                jwtAuthLink.getAuthKeyDetails().accessKey.isNotBlank() -> {
                    response.request.newBuilder()
                        .addHeader("Authorization", "Bearer ${jwtAuthLink.getAuthKeyDetails().accessKey}")
                        .build()
                }
                else -> {
                    null
                }
            } }
        .addInterceptor(HttpLoggingInterceptor().apply { setLevel(HttpLoggingInterceptor.Level.BODY) })
        .addInterceptor(Interceptor { chain ->
            val request = chain.request()
            val accessToken = jwtAuthLink.getAuthKeyDetails().accessKey
            if (accessToken.isNotBlank()) {
                val newRequest = request.newBuilder()
                    .addHeader("Authorization", "Bearer $accessToken")
                    .build()
                return@Interceptor chain.proceed(newRequest)
            }
            chain.proceed(request) })
        .build()


    fun newApiService(): ApiService = newRetrofitInstance().create<ApiService>()
}
