package com.example.busoffloader.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.math.BigDecimal

@JsonClass(generateAdapter = true)
data class BankingDeposit(val id: Long,
                          val description: String?,
                          val amount: BigDecimal,
                          @Json(name = "date_created") val dateCreated: String,
                          @Json(name = "date_modified") val dateModified: String,
                          @Json(name = "created_by") val createdBy: Long?,
                          @Json(name = "modified_by") val modifiedBy: Long?)