package com.example.busoffloader.data.remote

import com.example.busoffloader.data.BankingDepositDataSource
import com.example.busoffloader.data.OffloadDataSource
import com.example.busoffloader.data.VehicleDataSource
import com.example.busoffloader.data.authentication.AuthDataSource
import com.example.busoffloader.data.model.*
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import retrofit2.http.*
import java.math.BigDecimal


interface ApiService: AuthDataSource, OffloadDataSource, BankingDepositDataSource, VehicleDataSource {

    @POST("api/token/")
    @FormUrlEncoded
    override fun login(@Field("username") username: String,
                       @Field("password") password: String): Single<AuthJWTDetails>

    @GET("api/current_user/")
    override fun user(): Single<User>


    @GET("api/payments/")
    override fun paymentTransactions(@Query("created_by") createdById: Long?,
                                     @Query("vehicle") vehicleId: Long?): Single<List<Payment>>


    @POST("api/payments/")
    @FormUrlEncoded
    override fun newPaymentTransaction(@Field("transaction_type") transactionType: String,
                                       @Field("payment_description") paymentDescriptionId: Long,
                                       @Field("vehicle") vehicleId: Long?,
                                       @Field("amount") amount: BigDecimal): Single<Payment>


    @GET("api/payment_descriptions")
    override fun paymentsDescriptions(): Single<List<PaymentDescription>>


    @GET("api/banking_deposits/")
    override fun bankingDeposits(): Single<List<BankingDeposit>>


    @POST("api/banking_deposits/")
    @FormUrlEncoded
    override fun newBankingDeposit(@Field("description") description: String,
                                   @Field("amount") amount: BigDecimal): Single<BankingDeposit>

   @GET("api/buses/")
   override fun vehicles(): Single<List<Vehicle>>
}