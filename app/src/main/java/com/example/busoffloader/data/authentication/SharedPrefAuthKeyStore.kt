package com.example.busoffloader.data.authentication

import android.content.Context
import android.util.Log
import com.example.busoffloader.BuildConfig
import com.example.busoffloader.data.model.AuthJWTDetails

class SharedPrefAuthKeyStore(ctx: Context) : AuthJWTKeysStore {
    private val mApplicationContext: Context = ctx.applicationContext
    private val mSharedPreferences = mApplicationContext
        .getSharedPreferences(NAME, Context.MODE_PRIVATE)
    

    override fun putNewAuthKeyDetails(keys: AuthJWTDetails) {
        mSharedPreferences.edit()
            .putString(ACCESS_KEY, keys.accessKey)
            .putString(REFRESH_KEY, keys.refreshKey)
            .apply()
    }

    override fun getAuthKeyDetails(): AuthJWTDetails {
        return AuthJWTDetails(accessKey = mSharedPreferences.getString(ACCESS_KEY, "")!!,
                              refreshKey = mSharedPreferences.getString(REFRESH_KEY, "")!!)
    }

    override fun invalidateAuthDetails() {
        mSharedPreferences.edit()
            .clear()
            .apply()
    }

    override fun doAuthKeyDetailsExist(): Boolean {
        return mSharedPreferences.getString(ACCESS_KEY, "")!!.isNotBlank()
                && mSharedPreferences.getString(REFRESH_KEY, "")!!.isNotBlank()
    }

    companion object {
        private const val NAME = "busoffloader_data_SharedPrefAuthKeyStore"
        private const val ACCESS_KEY = "busoffloader_data_SharedPrefAuthKeyStore_ACCESS_KEY"
        private const val REFRESH_KEY = "busoffloader_data_SharedPrefAuthKeyStore_REFRESH_KEY"
    }
}