package com.example.busoffloader.data.authentication

import android.content.Context
import android.util.Log
import com.example.busoffloader.BuildConfig
import com.example.busoffloader.data.model.AuthJWTDetails
import com.example.busoffloader.data.model.User
import com.squareup.moshi.Moshi

class LocalDataSharedPrefs(ctx: Context) : LocalDataSource {
    private val mApplicationContext: Context = ctx.applicationContext
    private val mSharedPreferences = mApplicationContext
        .getSharedPreferences(NAME, Context.MODE_PRIVATE)
    private val _moshi = Moshi.Builder().build()
    private val _moshiUserAdapter = _moshi.adapter(User::class.java)

    override fun storeUserDetails(user: User) {
        mSharedPreferences.edit()
            .putString(_MAJOR_KEY, _moshiUserAdapter.toJson(user))
            .apply()
    }

    override fun getUserDetails(): User? {
        val userJsonString = mSharedPreferences.getString(_MAJOR_KEY, "")
        return if (userJsonString.isNullOrBlank()) null
        else _moshiUserAdapter.fromJson(userJsonString)
    }

    override fun clearUserDetails() {
        mSharedPreferences.edit()
            .clear()
            .apply()
    }

    companion object {
        private const val NAME = "busoffloader_data_authentication_LocalDataSharedPrefs"
        private const val _MAJOR_KEY = "busoffloader_data_authentication_LocalDataSharedPref_MAJOR_KEY"
    }
}