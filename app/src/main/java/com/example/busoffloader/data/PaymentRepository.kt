package com.example.busoffloader.data

import com.example.busoffloader.data.model.Payment
import com.example.busoffloader.data.model.PaymentDescription
import com.example.busoffloader.data.model.Vehicle
import io.reactivex.rxjava3.core.Single
import java.math.BigDecimal

class PaymentRepository(private val offloadDataSource: OffloadDataSource,
                        private val bankingDepositDataSource: BankingDepositDataSource,
                        private val vehicleDataSource: VehicleDataSource) {
    fun vehicles(): Single<List<Vehicle>> = vehicleDataSource.vehicles()

    fun paymentDescriptions(): Single<List<PaymentDescription>> =
        offloadDataSource.paymentsDescriptions()

    fun newPaymentTransaction(paymentDescription: PaymentDescription,
                              vehicle: Vehicle,
                              amount: BigDecimal): Single<Payment> =
        offloadDataSource.newPaymentTransaction(transactionType = "cash",
                                                paymentDescriptionId = paymentDescription.id,
                                                vehicleId = vehicle.id,
                                                amount = amount)

    fun paymentTransactions() =
        offloadDataSource.paymentTransactions(createdById = null,
                                              vehicleId = null)

    fun newBankingDeposit(description: String, amount: BigDecimal) =
        bankingDepositDataSource.newBankingDeposit(description, amount)

    fun bankingDeposits() = bankingDepositDataSource.bankingDeposits()
}