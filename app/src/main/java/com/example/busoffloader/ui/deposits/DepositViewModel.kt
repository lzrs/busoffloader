package com.example.busoffloader.ui.deposits

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.busoffloader.data.PaymentRepository
import com.example.busoffloader.data.model.BankingDeposit
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.plusAssign
import java.math.BigDecimal

class DepositViewModel(private val paymentRepository: PaymentRepository) : ViewModel() {
    private val _newBankingDepositResult = MutableLiveData<Result<BankingDeposit>>()
    val onNewBankingDepositResult = _newBankingDepositResult

    private val mCompositeDisposable: CompositeDisposable = CompositeDisposable()

    fun newBankingDeposit(description: String, amount: String) {
        mCompositeDisposable += paymentRepository.newBankingDeposit(description, amount = BigDecimal(amount))
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                           _newBankingDepositResult.postValue(Result.success(it))
                       },
                       {
                           _newBankingDepositResult.postValue(Result.failure(it))
                       })
    }

    override fun onCleared() {
        mCompositeDisposable.clear()
        super.onCleared()
    }
}