package com.example.busoffloader.ui.home

import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.busoffloader.R
import com.example.busoffloader.ui.deposits.DepositActivity
import com.google.android.material.floatingactionbutton.FloatingActionButton

class BankingDepositFragment : Fragment() {

    private lateinit var paymentViewModel: PaymentViewModel
    private val mBankingDepositAdapter: BankingDepositAdapter = BankingDepositAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        paymentViewModel = (requireActivity() as HomeActivity).viewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_banking_deposits, container, false)
        val recyclerView = root.findViewById<RecyclerView>(R.id.depositRecyclerview)
        val layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        recyclerView.addItemDecoration(DividerItemDecoration(requireContext(), layoutManager.orientation))
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = mBankingDepositAdapter

        val newDepositFAB = root.findViewById<FloatingActionButton>(R.id.depositFab)
        newDepositFAB.backgroundTintList = ColorStateList.valueOf(Color.YELLOW)
        newDepositFAB.setOnClickListener {
           startActivity(DepositActivity.newIntent(requireContext(), paymentViewModel.userDetailResult.value!!.getOrThrow()))
        }

        paymentViewModel.bankingDepositResults.observe(viewLifecycleOwner, Observer {
            if (it.isSuccess){
                mBankingDepositAdapter.refreshBankingDeposits(it.getOrThrow())
            }

            if (it.isFailure){
                Toast.makeText(requireContext().applicationContext,
                               "Error ${it.exceptionOrNull()}",
                               Toast.LENGTH_LONG)
                    .show()
            }
        })

        return root
    }

    companion object {
        private const val ARG_SECTION_NUMBER = "section_number"
        private const val TAG = "BankingDepositFragment"

        @JvmStatic
        fun newInstance(sectionNumber: Int): BankingDepositFragment {
            return BankingDepositFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }
}