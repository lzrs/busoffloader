package com.example.busoffloader.ui.login

import android.content.Context
import android.content.Intent
import androidx.lifecycle.Observer
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.app.TaskStackBuilder
import androidx.lifecycle.ViewModelProvider
import com.example.busoffloader.BusOffloaderApp
import com.example.busoffloader.ui.offload.OffloadActivity

import com.example.busoffloader.R
import com.example.busoffloader.data.model.User
import com.example.busoffloader.ui.LoginRepositoryViewModelFactory

class LoginActivity : AppCompatActivity() {

    private lateinit var loginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val loginRepository = (application as BusOffloaderApp).appContainer.loginRepository
        loginViewModel = ViewModelProvider(this, LoginRepositoryViewModelFactory(loginRepository))
            .get(LoginViewModel::class.java)

        if (loginViewModel.isUserLoggedIn()){
            switchToOffloadView()
        }

        setContentView(R.layout.activity_login)

        val username = findViewById<EditText>(R.id.username)
        val password = findViewById<EditText>(R.id.password)
        val login = findViewById<Button>(R.id.login)
        val loading = findViewById<ProgressBar>(R.id.loading)

        loginViewModel.loginFormState.observe(this@LoginActivity, Observer {
            val loginState = it ?: return@Observer

            // disable login button unless both username / password is valid
            login.isEnabled = loginState.isDataValid

            if (loginState.usernameError != null) {
                username.error = getString(loginState.usernameError)
            }
            if (loginState.passwordError != null) {
                password.error = getString(loginState.passwordError)
            }
        })

        loginViewModel.loginResult.observe(this@LoginActivity, Observer {
            val loginResult = it ?: return@Observer

            loading.visibility = View.GONE
            if (it.isFailure) {
                showLoginFailed(it.exceptionOrNull()!!)
            }
            if (it.isSuccess) {
                updateUiWithUser(LoggedInUserView(displayName = it.getOrThrow().username))
            }

            switchToOffloadView(it.getOrThrow())
        })

        username.afterTextChanged {
            loginViewModel.loginDataChanged(
                    username.text.toString(),
                    password.text.toString()
            )
        }

        password.apply {
            afterTextChanged {
                loginViewModel.loginDataChanged(
                        username.text.toString(),
                        password.text.toString()
                )
            }

            setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_DONE -> {
                        loading.visibility = View.VISIBLE
                        closeKeyboard()

                        loginViewModel.login(
                            username.text.toString(),
                            password.text.toString())
                    }
                }
                false
            }
        }

        login.setOnClickListener {
            closeKeyboard()
            
            loading.visibility = View.VISIBLE
            loginViewModel.login(username.text.toString(),
                                 password.text.toString())
        }
    }

    private fun switchToOffloadView(user: User? = null) {
        val offloadIntent = if (user != null) OffloadActivity.newIntent(this, user)
        else Intent(this, OffloadActivity::class.java)
        TaskStackBuilder.create(this)
            .addNextIntentWithParentStack(offloadIntent)
            .startActivities()
        //Complete and destroy login activity once successful
        finish()
    }

    private fun closeKeyboard(){
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private fun updateUiWithUser(model: LoggedInUserView) {
        val welcome = getString(R.string.welcome)
        val displayName = model.displayName
        // TODO : initiate successful logged in experience
//        save the access and refresh tokens
        Toast.makeText(
                applicationContext,
                "$welcome $displayName",
                Toast.LENGTH_LONG
        ).show()
    }

    private fun showLoginFailed(throwable: Throwable) {
        Toast.makeText(applicationContext, "Login Failed: ${throwable.message}", Toast.LENGTH_SHORT).show()
    }

    companion object{
        fun newIntent(context: Context): Intent{
            return Intent(context, LoginActivity::class.java)
                .apply {
                    flags += Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                }
        }
    }
}

/**
 * Extension function to simplify setting an afterTextChanged action to EditText components.
 */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}