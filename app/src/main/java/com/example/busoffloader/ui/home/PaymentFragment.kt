package com.example.busoffloader.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.busoffloader.R
import com.example.busoffloader.ui.offload.OffloadActivity
import com.google.android.material.floatingactionbutton.FloatingActionButton

class PaymentFragment : Fragment() {

    private lateinit var paymentViewModel: PaymentViewModel
    private val mPaymentAdapter: PaymentAdapter = PaymentAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        paymentViewModel = (requireActivity() as HomeActivity).viewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_payments, container, false)

        val recyclerView = root.findViewById<RecyclerView>(R.id.recyclerview)
        val layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        recyclerView.addItemDecoration(DividerItemDecoration(requireContext(), layoutManager.orientation))
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = mPaymentAdapter

        val payFab = root.findViewById<FloatingActionButton>(R.id.payFab)
        payFab.setOnClickListener {
            startActivity(OffloadActivity.newIntent(requireContext(), paymentViewModel.userDetailResult.value!!.getOrThrow()))
        }

        paymentViewModel.paymentResults.observe(viewLifecycleOwner, {
            if (it.isSuccess){
                mPaymentAdapter.refreshPayments(it.getOrThrow())
            }

            if (it.isFailure){
                Toast.makeText(requireContext().applicationContext,
                               "Error ${it.exceptionOrNull()}",
                               Toast.LENGTH_LONG)
                    .show()
            }
        })
        return root
    }

    companion object {
        private const val ARG_SECTION_NUMBER = "section_number"
        private const val TAG = "PaymentFragment"

        @JvmStatic
        fun newInstance(sectionNumber: Int): PaymentFragment {
            return PaymentFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }
}