package com.example.busoffloader.ui.home

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.busoffloader.BusOffloaderApp
import com.example.busoffloader.R
import com.example.busoffloader.ui.PaymentLoginRepositoryViewModelFactory
import com.example.busoffloader.ui.PaymentRepositoryViewModelFactory
import com.example.busoffloader.ui.login.LoginActivity

class HomeActivity : AppCompatActivity() {

    private lateinit var mPaymentViewModel: PaymentViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val homeBar: Toolbar = findViewById(R.id.homeBar)
        setSupportActionBar(homeBar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        val paymentRepository = (application as BusOffloaderApp).appContainer.paymentRepository
        val loginRepository = (application as BusOffloaderApp).appContainer.loginRepository
        mPaymentViewModel =
            ViewModelProvider(this, PaymentLoginRepositoryViewModelFactory(paymentRepository,
                                                                           loginRepository))
                .get(PaymentViewModel::class.java)

        val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager)
        val viewPager: ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(viewPager)
        val titleView: TextView = findViewById(R.id.title)

        mPaymentViewModel.user()
        mPaymentViewModel.userDetailResult.observe(this, Observer {
            if (it.isSuccess){
                titleView.text = it.getOrThrow().username
            }

            if (it.isFailure){
                Toast.makeText(applicationContext,
                               "Error ${it.exceptionOrNull()}",
                               Toast.LENGTH_LONG)
                    .show()
            }
        })
    }

    fun viewModel() = mPaymentViewModel

    override fun onResume() {
        mPaymentViewModel.payments()
        mPaymentViewModel.bankingDeposits()
        super.onResume()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_home, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.action_log_out -> {
                mPaymentViewModel.logOut()
                startActivity(LoginActivity.newIntent(this))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}