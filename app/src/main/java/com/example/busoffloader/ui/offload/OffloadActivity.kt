package com.example.busoffloader.ui.offload

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NavUtils
import androidx.lifecycle.ViewModelProvider
import com.example.busoffloader.BusOffloaderApp
import com.example.busoffloader.R
import com.example.busoffloader.data.model.PaymentDescription
import com.example.busoffloader.data.model.User
import com.example.busoffloader.data.model.Vehicle
import com.example.busoffloader.ui.PaymentLoginRepositoryViewModelFactory
import com.example.busoffloader.ui.login.afterTextChanged


class OffloadActivity : AppCompatActivity() {
    private lateinit var mViewModel: OffloadViewModel
    private val printingDialogFragment = PrintingDialogFragment.newInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_offload)

        setSupportActionBar(findViewById(R.id.my_toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val vehicles: Spinner = findViewById(R.id.vehicles_spinner)
        val categories: Spinner = findViewById(R.id.categories_spinner)
        val amountEditText: EditText = findViewById(R.id.amount)
        val submitButton: Button = findViewById(R.id.submit)

        fun updateSubmitButton() {
            submitButton.isEnabled = amountEditText.text.toString().isNotBlank()
                    && categories.selectedItem != null
                    && vehicles.selectedItem != null
        }

        fun clearAmount() {
            amountEditText.setText("")
        }

        val paymentRepository = (application as BusOffloaderApp).appContainer.paymentRepository
        val loginRepository = (application as BusOffloaderApp).appContainer.loginRepository
        mViewModel = ViewModelProvider(this,
                                       PaymentLoginRepositoryViewModelFactory(paymentRepository = paymentRepository,
                                                                              loginRepository = loginRepository))
            .get(OffloadViewModel::class.java)

        if (!intent.hasExtra(USER_EXTRA)) {
            mViewModel.user()
        } else {
            mViewModel.storeUser(intent.getParcelableExtra<User>(USER_EXTRA)!!)
        }


        mViewModel.fetchVehicles()
        mViewModel.fetchPaymentDescriptions()
        mViewModel.vehiclesResult.observe(this, {
            if (it.isSuccess) {
                vehicles.adapter =
                    ArrayAdapter(this, android.R.layout.simple_list_item_1, it.getOrThrow())

                vehicles.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(parent: AdapterView<*>?,
                                                view: View?,
                                                position: Int,
                                                id: Long) {
                        updateSubmitButton()
                    }

                    override fun onNothingSelected(parent: AdapterView<*>?) {
                        Log.d(TAG, "vehicles nothing selected")
                    }
                }
            }

            if (it.isFailure) {
                Toast.makeText(applicationContext,
                               "Error ${it.exceptionOrNull()}",
                               Toast.LENGTH_LONG)
                    .show()
            }
        })

        mViewModel.paymentDescriptionResult.observe(this, {
            if (it.isSuccess) {
                categories.adapter =
                    ArrayAdapter(this, android.R.layout.simple_list_item_1, it.getOrThrow())

                categories.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(parent: AdapterView<*>?,
                                                view: View?,
                                                position: Int,
                                                id: Long) {
                        updateSubmitButton()
                    }

                    override fun onNothingSelected(parent: AdapterView<*>?) {
                        Log.d(TAG, "categories nothing selected")
                    }

                }
            }

            if (it.isFailure) {
                Toast.makeText(applicationContext,
                               "Error ${it.exceptionOrNull()}",
                               Toast.LENGTH_LONG)
                    .show()
            }
        })

        mViewModel.onNewPayment.observe(this, {
            submitButton.visibility = View.VISIBLE
            if (it.isSuccess) {
                val receipt = it.getOrThrow()
                printReceipt(receipt)

                Toast.makeText(applicationContext,
                               "Success pay ${receipt.amount} - ${receipt.registration}",
                               Toast.LENGTH_LONG)
                    .show()
                clearAmount()
            }

            if (it.isFailure) {
                Toast.makeText(applicationContext,
                               "Error ${it.exceptionOrNull()}",
                               Toast.LENGTH_LONG)
                    .show()
            }
        })

        amountEditText.afterTextChanged { updateSubmitButton() }

        submitButton.setOnClickListener {
            val payDescription = categories.selectedItem as PaymentDescription
            val vehicle = vehicles.selectedItem as Vehicle
            val amountToPay = amountEditText.text.toString()

            it.visibility = View.INVISIBLE
            mViewModel.newPayment(payDescription, vehicle, amountToPay)
        }

    }

    private fun printReceipt(receipt: Receipt){
        printingDialogFragment.show(supportFragmentManager, "printingDialogFragment")
        BusOffloaderApp.printText(1, 0, receipt.toString())
        printingDialogFragment.dismiss()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                NavUtils.navigateUpFromSameTask(this)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        private const val TAG = "OffloadActivity"
        private const val USER_EXTRA = "ui.offloadActivity"

        fun newIntent(context: Context, user: User): Intent {
            return Intent(context, OffloadActivity::class.java)
                .apply {
                    putExtra(USER_EXTRA, user)
                }
        }
    }
}