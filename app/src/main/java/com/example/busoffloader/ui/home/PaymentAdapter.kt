package com.example.busoffloader.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.example.busoffloader.R
import com.example.busoffloader.data.model.Payment
import java.util.*

class PaymentAdapter : RecyclerView.Adapter<PaymentAdapter.PaymentViewHolder>() {
    class PaymentViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val paymentDescriptionView =
            itemView.findViewById<AppCompatTextView>(R.id.paymentDescription)
        private val amountView = itemView.findViewById<AppCompatTextView>(R.id.amount)
        private val transactionTypeView =
            itemView.findViewById<AppCompatTextView>(R.id.transactionType)
        private val vehicleView = itemView.findViewById<AppCompatTextView>(R.id.vehicle)
        private val datePaidView = itemView.findViewById<AppCompatTextView>(R.id.datePaid)

        fun bind(payment: Payment) {
            amountView.text = "KES ${payment.amount}"
            transactionTypeView.text = payment.transactionType.toUpperCase(Locale.getDefault())
            datePaidView.text = payment.dateCreated
            paymentDescriptionView.text = "${payment.paymentDescription?.name ?: "Type of payment"}".capitalize(Locale.getDefault())
            vehicleView.text = "${payment.vehicleRep?.registration ?: "Registration Number"}"
        }
    }

    fun refreshPayments(payments: List<Payment>){
        _payments = payments
        notifyDataSetChanged()
    }

    private var _payments = listOf<Payment>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentViewHolder {
        val paymentView = LayoutInflater.from(parent.context)
            .inflate(R.layout.content_payment_item, parent, false)
        return PaymentViewHolder(paymentView)
    }

    override fun onBindViewHolder(holder: PaymentViewHolder, position: Int) {
        holder.bind(_payments[position])
    }

    override fun getItemCount(): Int = _payments.size

    companion object{
        private const val TAG = "PaymentAdapter"
    }
}