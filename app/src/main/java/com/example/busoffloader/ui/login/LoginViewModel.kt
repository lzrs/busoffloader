package com.example.busoffloader.ui.login

import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.busoffloader.R
import com.example.busoffloader.data.authentication.LoginRepository
import com.example.busoffloader.data.model.User
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.functions.Function
import io.reactivex.rxjava3.kotlin.plusAssign

class LoginViewModel(private val loginRepository: LoginRepository) : ViewModel() {

    private val _loginForm = MutableLiveData<LoginFormState>()
    val loginFormState: LiveData<LoginFormState> = _loginForm

    private val _loginResult = MutableLiveData<Result<User>>()
    val loginResult: LiveData<Result<User>> = _loginResult

    private val mCompositeDisposable: CompositeDisposable = CompositeDisposable()

    fun login(username: String, password: String) {
        // can be launched in a separate asynchronous job
        mCompositeDisposable += loginRepository.login(username, password)
            .flatMapPublisher(Function {
                loginRepository.getUser()
            })
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                           _loginResult.postValue(Result.success(it))
                       },
                       {
                           _loginResult.postValue(Result.failure(it))
                       })
    }

    fun isUserLoggedIn(): Boolean = loginRepository.isLoggedIn()

    fun loginDataChanged(username: String, password: String) {
        if (!isUserNameValid(username)) {
            _loginForm.postValue(LoginFormState(usernameError = R.string.invalid_username))
        } else if (!isPasswordValid(password)) {
            _loginForm.postValue(LoginFormState(passwordError = R.string.invalid_password))
        } else {
            _loginForm.postValue(LoginFormState(isDataValid = true))
        }
    }

    override fun onCleared() {
        mCompositeDisposable.clear()
        super.onCleared()
    }

    // A placeholder username validation check
    private fun isUserNameValid(username: String): Boolean {
        return if (username.contains('@')) {
            Patterns.EMAIL_ADDRESS.matcher(username).matches()
        } else {
            username.isNotBlank()
        }
    }

    // A placeholder password validation check
    private fun isPasswordValid(password: String): Boolean {
        return password.length > 5
    }
}