package com.example.busoffloader.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.example.busoffloader.R
import com.example.busoffloader.data.model.BankingDeposit

class BankingDepositAdapter : RecyclerView.Adapter<BankingDepositAdapter.BankingDepositViewHolder>(){
    class BankingDepositViewHolder(view: View): RecyclerView.ViewHolder(view){
        private val depositAmountView = itemView.findViewById<AppCompatTextView>(R.id.amount)
        private val descriptionNoteView = itemView.findViewById<AppCompatTextView>(R.id.descriptionNote)
        private val dateCreatedView = itemView.findViewById<AppCompatTextView>(R.id.dateCreated)

        fun bind(deposit: BankingDeposit){
            depositAmountView.text = "KES ${deposit.amount}"
            descriptionNoteView.text = deposit.description
            dateCreatedView.text = deposit.dateCreated
        }
    }

    fun refreshBankingDeposits(bankingDeposits: List<BankingDeposit>){
        _bankingDeposits = bankingDeposits
        notifyDataSetChanged()
    }

    private var _bankingDeposits = listOf<BankingDeposit>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BankingDepositViewHolder {
        val depositItemView  = LayoutInflater.from(parent.context)
            .inflate(R.layout.content_banking_deposit_item, parent, false)
        return BankingDepositViewHolder(depositItemView)
    }

    override fun onBindViewHolder(holder: BankingDepositViewHolder, position: Int) =
        holder.bind(_bankingDeposits[position])

    override fun getItemCount(): Int = _bankingDeposits.size
}