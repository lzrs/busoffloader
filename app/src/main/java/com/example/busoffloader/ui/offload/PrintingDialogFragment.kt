package com.example.busoffloader.ui.offload

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.example.busoffloader.R

class PrintingDialogFragment : DialogFragment() {
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_printing, container, false)
    }

    companion object{
        fun newInstance() = PrintingDialogFragment()
    }
}