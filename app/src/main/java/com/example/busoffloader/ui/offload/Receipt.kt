package com.example.busoffloader.ui.offload

data class Receipt(val amount: String,
                   val username: String,
                   val fleetNumber: String,
                   val registration: String){
    override fun toString(): String {
        return "KES $amount received from $username for vehicle $registration fleet number $fleetNumber \n"
    }
}