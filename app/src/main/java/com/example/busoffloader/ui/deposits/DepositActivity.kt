package com.example.busoffloader.ui.deposits

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.app.NavUtils
import androidx.lifecycle.ViewModelProvider
import com.example.busoffloader.BusOffloaderApp
import com.example.busoffloader.R
import com.example.busoffloader.data.model.User
import com.example.busoffloader.ui.PaymentRepositoryViewModelFactory
import com.example.busoffloader.ui.login.afterTextChanged

class DepositActivity : AppCompatActivity() {
    private lateinit var mViewModel: DepositViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_deposit)

        setSupportActionBar(findViewById(R.id.my_toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val depositDescriptionView: AppCompatEditText = findViewById(R.id.depositDescription)
        val amountEditText: EditText = findViewById(R.id.amount)
        val submitButton: Button = findViewById(R.id.submit)

        fun updateSubmitButton(){
            submitButton.isEnabled = amountEditText.text.toString().isNotBlank()
        }

        fun clearAmount(){
            depositDescriptionView.setText("")
            amountEditText.setText("")
        }

        val paymentRepository = (application as BusOffloaderApp).appContainer.paymentRepository
        mViewModel = ViewModelProvider(this, PaymentRepositoryViewModelFactory(paymentRepository = paymentRepository))
            .get(DepositViewModel::class.java)

        mViewModel.onNewBankingDepositResult.observe(this, {
            if (it.isSuccess){
                clearAmount()

                val successDeposit = it.getOrThrow()
                Toast.makeText(applicationContext,
                               "Success deposit ${successDeposit.amount} for ${successDeposit.description}",
                               Toast.LENGTH_LONG)
                    .show()
            }

            if (it.isFailure){
                Toast.makeText(applicationContext,
                               "Error ${it.exceptionOrNull()}",
                               Toast.LENGTH_LONG)
                    .show()
            }
        })

        amountEditText.afterTextChanged { updateSubmitButton() }

        submitButton.setOnClickListener {
            val descriptionNote = depositDescriptionView.text.toString()
            val amountToPay = amountEditText.text.toString()

            mViewModel.newBankingDeposit(descriptionNote, amountToPay)
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            android.R.id.home -> {
                NavUtils.navigateUpFromSameTask(this)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object{
        private const val TAG = "DepositActivity"
        private const val EXXTRA_USER = "ui.deposits.DepositActivity"

        fun newIntent(context: Context, user: User): Intent {
            return Intent(context, DepositActivity::class.java)
                .apply {
                    putExtra(EXXTRA_USER, user)
                }
        }
    }
}