package com.example.busoffloader.ui.offload

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.busoffloader.data.PaymentRepository
import com.example.busoffloader.data.authentication.LoginRepository
import com.example.busoffloader.data.model.PaymentDescription
import com.example.busoffloader.data.model.User
import com.example.busoffloader.data.model.Vehicle
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.plusAssign
import java.math.BigDecimal

class OffloadViewModel(private val paymentRepository: PaymentRepository,
                       private val loginRepository: LoginRepository) : ViewModel() {
    private val _vehiclesData = MutableLiveData<Result<List<Vehicle>>>()
    val vehiclesResult: LiveData<Result<List<Vehicle>>> = _vehiclesData

    private val _paymentDescriptionsData = MutableLiveData<Result<List<PaymentDescription>>>()
    val paymentDescriptionResult: LiveData<Result<List<PaymentDescription>>> =
        _paymentDescriptionsData

    private val _newPaymentResultData = MutableLiveData<Result<Receipt>>()
    val onNewPayment: LiveData<Result<Receipt>> = _newPaymentResultData

    private val mCompositeDisposable: CompositeDisposable = CompositeDisposable()

    private val _userDetailData = MutableLiveData<Result<User>>()
    val userDetailResult: LiveData<Result<User>> = _userDetailData

    fun user() {
        mCompositeDisposable += loginRepository.getUser()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                           _userDetailData.postValue(Result.success(it))
                       },
                       {
                           _userDetailData.postValue(Result.failure(it))
                       })
    }

    fun storeUser(user: User){
        _userDetailData.postValue(Result.success(user))
    }

    fun fetchVehicles() {
        mCompositeDisposable += paymentRepository.vehicles()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                           _vehiclesData.postValue(Result.success(it))
                       },
                       {
                           _vehiclesData.postValue(Result.failure(it))
                       })
    }

    fun fetchPaymentDescriptions() {
        mCompositeDisposable += paymentRepository.paymentDescriptions()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                           _paymentDescriptionsData.postValue(Result.success(it))
                       },
                       {
                           _paymentDescriptionsData.postValue(Result.failure(it))
                       })
    }

    fun newPayment(description: PaymentDescription, vehicle: Vehicle, amount: String) {
        mCompositeDisposable += paymentRepository
            .newPaymentTransaction(description, vehicle, amount = BigDecimal(amount))
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({payment ->
                           val receipt = Receipt(amount = payment.amount.toString(),
                                                 username = userDetailResult.value?.getOrNull()?.username ?: "User",
                                                 fleetNumber = vehicle.fleetNumber,
                                                 registration = vehicle.registration)
                           _newPaymentResultData.postValue(Result.success(receipt))
                       },
                       {
                           _newPaymentResultData.postValue(Result.failure(it))
                       })
    }

    override fun onCleared() {
        mCompositeDisposable.clear()
        super.onCleared()
    }
}
