package com.example.busoffloader.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.busoffloader.data.PaymentRepository
import com.example.busoffloader.data.authentication.LoginRepository
import com.example.busoffloader.data.model.BankingDeposit
import com.example.busoffloader.data.model.Payment
import com.example.busoffloader.data.model.User
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.functions.Function3
import io.reactivex.rxjava3.kotlin.plusAssign

class PaymentViewModel(private val paymentRepository: PaymentRepository,
                       private val loginRepository: LoginRepository) : ViewModel() {
    private val mCompositeDisposable: CompositeDisposable = CompositeDisposable()
    private val _payments = MutableLiveData<Result<List<Payment>>>()
    val paymentResults: LiveData<Result<List<Payment>>> = _payments
    private val _bankingDeposits = MutableLiveData<Result<List<BankingDeposit>>>()
    val bankingDepositResults: LiveData<Result<List<BankingDeposit>>> = _bankingDeposits

    private val _userDetailData = MutableLiveData<Result<User>>()
    val userDetailResult: LiveData<Result<User>> = _userDetailData

    fun user() {
        mCompositeDisposable += loginRepository.getUser()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                           _userDetailData.postValue(Result.success(it))
                       },
                       {
                           _userDetailData.postValue(Result.failure(it))
                       })
    }

    fun logOut() {
        loginRepository.logout()
    }

    fun payments() {
        mCompositeDisposable += Flowable
            .combineLatest(paymentRepository.vehicles().toFlowable(),
                           paymentRepository.paymentDescriptions()
                               .toFlowable(),
                           paymentRepository.paymentTransactions()
                               .toFlowable(),
                           Function3 { vehicles, descriptions, payments ->
                               return@Function3 payments
                                   .map { payment ->
                                       payment.vehicleRep =
                                           vehicles.find { vehicle -> vehicle.id == payment.vehicleId }
                                       payment.paymentDescription =
                                           descriptions.find { paymentDescription -> paymentDescription.id == payment.paymentDescriptionId }

                                       return@map payment
                                   }
                           })
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                           _payments.postValue(Result.success(it))
                       },
                       {
                           _payments.postValue(Result.failure(it))
                       })

    }

    fun bankingDeposits() {
        mCompositeDisposable += paymentRepository.bankingDeposits()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                           _bankingDeposits.postValue(Result.success(it))
                       },
                       {
                           _bankingDeposits.postValue(Result.failure(it))
                       })
    }

    override fun onCleared() {
        mCompositeDisposable.clear()
        super.onCleared()
    }
}