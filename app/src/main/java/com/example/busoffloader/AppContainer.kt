package com.example.busoffloader

import android.content.Context
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import com.example.busoffloader.data.PaymentRepository
import com.example.busoffloader.data.authentication.LocalDataSharedPrefs
import com.example.busoffloader.data.authentication.LoginRepository
import com.example.busoffloader.data.authentication.SharedPrefAuthKeyStore
import com.example.busoffloader.data.remote.ApiServiceProvider
import com.example.busoffloader.ui.login.LoginActivity

/**
 * https://developer.android.com/training/dependency-injection/manual#dependencies-container**/
class AppContainer(appContext: Context) {
    private val authSharedPrefStore = SharedPrefAuthKeyStore(ctx = appContext)
    private val localDataSource = LocalDataSharedPrefs(ctx = appContext)
    private val apiServiceProvider = ApiServiceProvider(jwtAuthLink = authSharedPrefStore,
    onAuthExpired = {
        authSharedPrefStore.invalidateAuthDetails()
        appContext.startActivity(LoginActivity.newIntent(appContext))
        Toast.makeText(appContext, "Session expired", Toast.LENGTH_SHORT)
            .show()
    })
    private val apiServiceDataSource = apiServiceProvider.newApiService()

    val loginRepository: LoginRepository = LoginRepository(
        dataSource = apiServiceDataSource,
        localAuthStore = authSharedPrefStore,
        localDataStore = localDataSource)

    val paymentRepository: PaymentRepository = PaymentRepository(
        offloadDataSource = apiServiceDataSource,
        bankingDepositDataSource = apiServiceDataSource,
        vehicleDataSource = apiServiceDataSource)
}